#include <MIDI.h>

MIDI_CREATE_DEFAULT_INSTANCE();

// 6 = Green LED, 7 = Red LED
static const byte ledGreenPin = 6;
static const byte ledRedPin = 7;
static const byte ledTime = 50;

bool ledGreenEnable = false;
bool ledRedEnable = false;

static const byte buttonFunction = 4;
static const byte buttonChannel = 3;
static const byte buttonAction = 2;

static const byte buttonDebounceDelay = 50;

static const byte xgVelSenseInterval = 100;
static const byte xgVelSenseDepthPotPin = 0;
static const byte xgVelSenseOffsetPotPin = 1;

// 16 bits = 16 channels (0 = off, 1 = on)
short xgVelSenseChannels = 0;

static const byte thruVelSenseInterval = 100;
static const byte thruVelSenseDepthPotPin = 0;
static const byte thruVelSenseOffsetPotPin = 1;

// 16 bits = 16 channels (0 = off, 1 = on)
short thruVelSenseChannels = 0;

byte thruVelSenseDepth = 128;
short thruVelSenseOffset = 0;

static unsigned long ledGreenOnMillis = 0;
static unsigned long ledRedOnMillis = 0;

bool doRolandSysExChecksum = true;

// Add the checksum to a Roland system exclusive message
// and send it
void rolandSysExChecksum(byte* array, unsigned int size)
{
	static unsigned int sum = 0;
	static byte checksum = 0;
	static byte idx = 0;

	// Let's find the start of data bytes
	// It is after the Model ID and the Op Code
	// Model ID can be "XX", "00 XX" or "00 00 XX"
	if (array[3] == 0) {
		if (array[4] == 0) {
			if (array[5] == 0) {
				// Error ?
				return;
			} else {
				idx = 7;
			}
		} else {
			idx = 6;
		}
	} else {
		idx = 5;
	}

	// Sum all data bytes
	for (sum = 0; idx < size-1; ++idx) {
		sum += array[idx];
	}

	// Compute the checksum
	checksum = 128 - (sum % 128);

	// Replace SysEx ending byte (0xF7) by checksum
	if (array[size-1] == 0xf7) {
		array[size-1] = checksum;
	}

	// Send the SysEx message
	// Since we changed the ending byte (0xF7) by the checksum,
	// we also don't send the first byte by (0xF0)
	// and let the sendSysEx function do it
	MIDI.sendSysEx(size-1, &array[1], false);

	// Disable MIDI Thru so the message is not forwarded
	MIDI.turnThruOff();
	// MIDI Thru is then immediatly re-enabled in loop()

	ledRedEnable = true;

}

void customSystemExclusive(byte* array, unsigned int size)
{
	// array[1] = 0x7D

	switch (array[2]) {

		case 0x00:
			// Roland SysEx Cheksums
			// F0 7D 00 <val> F7
			if (array[3] == 0x00) {
				doRolandSysExChecksum = false;
			} else if (array[3] == 0x01) {
				doRolandSysExChecksum = true;
			} else {
				// Unknown value
				ledRedEnable = true;
			}
			break;

		case 0x01:
			// XG Velocity Sense
			// Acutally nothing to do
			// F0 7D 01 <...> F7
			break;

		case 0x02:
			// Thru Velocity Sense
			if (array[3] == 0x00) {
				// Values
				// Depth:  F0 7D 02 00 00 <val> F7
				// Offset: F0 7D 02 00 01 <val> F7
				if (array[4] == 0x00) {
					// Thru Velocity Sense Depth
					// Get 0 to 127, set 64 to 191
					thruVelSenseDepth = 64 + array[5];
				} else if (array[4] == 0x01) {
					// Thru Velocity Sense Offset
					// Get 0 to 127, set -64 to 63
					thruVelSenseOffset = array[5] - 64;
				} else {
					ledRedEnable = true;
				}
			} else if (array[3] == 0x01) {
				// Channels
				// F0 7D 02 01 <chan> <val> F7
				if (array[4] >= 1 && array[4] <= 16) {
					if (array[5] == 0x00) {
						// Set channel bit to 0
						thruVelSenseChannels &= ~(1 << (array[4] - 1));
					} else if (array[5] == 0x01) {
						// Set channel bit to 1
						thruVelSenseChannels |= (1 << (array[4] - 1));
					} else {
						ledRedEnable = true;
					}
				} else {
					ledRedEnable = true;
				}
			} else {
				ledRedEnable = true;
			}
			break;

		default:
			ledRedEnable = true;
	}

	// Disable MIDI Thru so the message is not forwarded
	MIDI.turnThruOff();
	// MIDI Thru is then immediatly re-enabled in loop()

	ledGreenEnable = true;
}

// Callback to handle System Exclusive messages
void handleSystemExclusive(byte* array, unsigned int size)
{
	switch (array[1]) {
		case 0x41:
			// Roland SysEx
			if (doRolandSysExChecksum) {
				rolandSysExChecksum(array, size);
			};
			break;

		case 0x7D:
			// Reserved ID for non-commercial use
			// Use it to allow configuration through custom
			// MIDI System exclusive messages
			customSystemExclusive(array, size);
			break;
	}
}

byte thruVelocitySense(byte velocity)
{
	// Store new velocity in 16 bit signed
	// as 0 - 16383 = 0 - 127
	// Computed values will range from
	// Min: (1 - 32) * 192 = -5952 (undeflow, don't send noteOn)
	// Max: (127 + 32) * 192 = 30528 (overflow, send velocity=127)
	static int newVelocity = 0;

	newVelocity = (velocity + thruVelSenseOffset) * thruVelSenseDepth;

	if (newVelocity > 16383) {
		// Velocity overflow
		ledRedEnable = true;
		return(127);

	} else if (newVelocity <= 0) {
		// Velocity underflow
		ledRedEnable = true;
		return(0);
	}

	return(newVelocity >> 7);
}

// Callback to handle Note On messages
void handleNoteOn(byte channel, byte note, byte velocity) {

	static bool changed = false;

	// thruVelocitySense
	if ((thruVelSenseChannels & (1 << (channel - 1))) && velocity > 0) {
		velocity = thruVelocitySense(velocity);
		changed = true;
	}

	if (changed) {
		MIDI.sendNoteOn(note, velocity, channel);

		// Disable MIDI Thru so the original message is not forwarded
		MIDI.turnThruOff();
		// MIDI Thru is then immediatly re-enabled in loop()
	}
}

// Set parameters for Thru Velocity Sense
void thruVelocitySenseSetParameters() {

	static unsigned long thruVelSenseReadMillis = 0;
	static byte thruVelSenseDepthNew = 0;
	static short thruVelSenseOffsetNew = 0;

	// Only read values every thruVelSenseInterval (ms)
	if (millis() - thruVelSenseReadMillis >= thruVelSenseInterval) {

		// Save last read time
		thruVelSenseReadMillis = millis();

		// analogRead: 10 bit,  0 => 1023

		// Manage depth between 64 and 192 (50 and 150%)
		thruVelSenseDepthNew = 64 + ( analogRead(thruVelSenseDepthPotPin) >> 3 );

		// Manage offset between -32 and +31
		thruVelSenseOffsetNew = ( analogRead(thruVelSenseOffsetPotPin) >> 4 ) - 32;

		if (thruVelSenseDepthNew != thruVelSenseDepth) {

			thruVelSenseDepth = thruVelSenseDepthNew;

			if (thruVelSenseDepth == 128) {
				// Show that we are centered
				ledRedEnable = true;
			}
		}

		if (thruVelSenseOffsetNew != thruVelSenseOffset) {

			thruVelSenseOffset = thruVelSenseOffsetNew;

			if (thruVelSenseOffset == 0) {
				// Show that we are centered
				ledRedEnable = true;
			}
		}
	}
}

// Send XG velocity sense depth and offset
// Values from potentiometers
void xgVelocitySense(byte channel) {

	// Start with 0, so at first run, we send initial values
	static byte xgVelSenseDepthCur = 0;
	static byte xgVelSenseOffsetCur = 0;

	static byte xgVelSenseDepthNew = 0;
	static byte xgVelSenseOffsetNew = 0;

	// Keep current channel to force sending when channel change
	static byte currentChannel = 1;

	// Depth:  FO - 43 10 4C O8 <channel> 0C <value> - F7
	// Offset: FO - 43 10 4C O8 <channel> 0D <value> - F7

	static byte xgVelSenseDepthSysex[7] = {0x43, 0x10, 0x4C, 0x08, 0x01, 0x0C, 0x40};
	static byte xgVelSenseOffsetSysex[7] = {0x43, 0x10, 0x4C, 0x08, 0x01, 0x0D, 0x40};

	static unsigned long xgVelSenseReadMillis = 0;

	// Only read values every xgVelSenseInterval (ms)
	if (millis() - xgVelSenseReadMillis >= xgVelSenseInterval) {

		// Save last read time
		xgVelSenseReadMillis = millis();

		// Manage values between 48 and 80
		// to get better potentiometers precision on usefull values
		xgVelSenseDepthNew = 48 + ( analogRead(xgVelSenseDepthPotPin) >> 5 );
		xgVelSenseOffsetNew = 48 + ( analogRead(xgVelSenseOffsetPotPin) >> 5 );

		if (xgVelSenseDepthNew != xgVelSenseDepthCur
				|| channel != currentChannel) {

			xgVelSenseDepthCur = xgVelSenseDepthNew;
			xgVelSenseDepthSysex[6] = xgVelSenseDepthNew;
			xgVelSenseDepthSysex[4] = channel;

			MIDI.sendSysEx(7, xgVelSenseDepthSysex, false);

			ledGreenEnable = true;

			if (xgVelSenseDepthCur == 64) {
				// Show that we are centered
				ledRedEnable = true;
			}
		}

		if (xgVelSenseOffsetNew != xgVelSenseOffsetCur
				|| channel != currentChannel) {

			xgVelSenseOffsetCur = xgVelSenseOffsetNew;
			xgVelSenseOffsetSysex[6] = xgVelSenseOffsetNew;
			xgVelSenseOffsetSysex[4] = channel;

			MIDI.sendSysEx(7, xgVelSenseOffsetSysex, false);

			ledGreenEnable = true;

			if (xgVelSenseOffsetCur == 64) {
				// Show that we are centered
				ledRedEnable = true;
			}
		}

		currentChannel = channel;
	}
}


void setup()
{
	pinMode(ledGreenPin, OUTPUT);
	digitalWrite(ledGreenPin, HIGH);

	pinMode(ledRedPin, OUTPUT);
	digitalWrite(ledRedPin, HIGH);

	pinMode(buttonFunction, INPUT_PULLUP);
	pinMode(buttonChannel, INPUT_PULLUP);
	pinMode(buttonAction, INPUT_PULLUP);

	MIDI.begin(MIDI_CHANNEL_OMNI);

	MIDI.setHandleSystemExclusive(handleSystemExclusive);
	MIDI.setHandleNoteOn(handleNoteOn);
}

void loop()
{

	static int buttonFunctionRead = HIGH;
	static int buttonFunctionLast = HIGH;
	static int buttonFunctionState = HIGH;
	static unsigned long buttonFunctionDebounceTime = 0;

	static int buttonChannelRead = HIGH;
	static int buttonChannelLast = HIGH;
	static int buttonChannelState = HIGH;
	static unsigned long buttonChannelDebounceTime = 0;

	static int buttonActionRead = HIGH;
	static int buttonActionLast = HIGH;
	static int buttonActionState = HIGH;
	static unsigned long buttonActionDebounceTime = 0;

	static byte currentFunction = 0;
	static byte currentChannel = 1;

	ledGreenEnable = false;
	ledRedEnable = false;

	if (MIDI.read())
	{
		// Enable Thru that might have been disabled by a callback function
		MIDI.turnThruOn();

		// Enable Thru LED
		ledGreenEnable = true;
	}

	switch (currentFunction) {
		case 0:
			// Roland SysEx
			// Nothing to do
			break;

		case 1:
			// XG Velocity Sense
			if ((xgVelSenseChannels & (1 << (currentChannel - 1)))) {
				xgVelocitySense(currentChannel);
			}
			break;

		case 2:
			// Thru Velocity Sense
			thruVelocitySenseSetParameters();
			break;
	}

	// Function button
	buttonFunctionRead = digitalRead(buttonFunction);

	if (buttonFunctionRead != buttonFunctionLast) {
		buttonFunctionDebounceTime = millis();
	}
	if ((millis() - buttonFunctionDebounceTime) > buttonDebounceDelay) {

		if (buttonFunctionRead != buttonFunctionState) {
			buttonFunctionState = buttonFunctionRead;

			if (buttonFunctionState == LOW) {

				// Switch function
				if (currentFunction == 2) {
					currentFunction = 0;
					ledRedEnable = 1;
				} else {
					currentFunction++;
				}
			}
		}
	}
	buttonFunctionLast = buttonFunctionRead;

	// Channel button
	buttonChannelRead = digitalRead(buttonChannel);

	if (buttonChannelRead != buttonChannelLast) {
		buttonChannelDebounceTime = millis();
	}
	if ((millis() - buttonChannelDebounceTime) > buttonDebounceDelay) {

		if (buttonChannelRead != buttonChannelState) {
			buttonChannelState = buttonChannelRead;

			if (buttonChannelState == LOW) {

				// Switch channel
				if (currentChannel == 16) {
					currentChannel = 1;
					ledRedEnable = true;
				} else {
					currentChannel++;
				}
				switch (currentFunction) {

					case 0:
						// Roland SysEx
						// Nothing to do
						break;
					case 1:
						// XG Velocity Sense
						if (xgVelSenseChannels & (1 << (currentChannel - 1))) {
							ledGreenEnable = true;
						}
						break;
					case 2:
						// Thru Velocity Sense
						if (thruVelSenseChannels & (1 << (currentChannel - 1))) {
							ledGreenEnable = true;
						}
						break;
				}
			}
		}
	}
	buttonChannelLast = buttonChannelRead;

	// Action button
	buttonActionRead = digitalRead(buttonAction);

	if (buttonActionRead != buttonActionLast) {
		buttonActionDebounceTime = millis();
	}
	if ((millis() - buttonActionDebounceTime) > buttonDebounceDelay) {

		if (buttonActionRead != buttonActionState) {
			buttonActionState = buttonActionRead;

			if (buttonActionState == LOW) {

				switch (currentFunction) {

					case 0:
						// Roland SysEx
						doRolandSysExChecksum = !doRolandSysExChecksum;
						break;
					case 1:
						// XG Velocity Sense
						xgVelSenseChannels ^= 1 << (currentChannel - 1);
						break;
					case 2:
						// Thru Velocity Sense
						thruVelSenseChannels ^= 1 << (currentChannel - 1);
						break;
				}

				ledGreenEnable = true;
			}
		}
	}
	buttonActionLast = buttonActionRead;

	// Green LED
	if (ledGreenEnable) {
		if (ledGreenOnMillis == 0) {
			digitalWrite(ledGreenPin, LOW);
		}
		ledGreenOnMillis = millis();
	} else {
		if (ledGreenOnMillis != 0) {
			if (millis() - ledGreenOnMillis >= ledTime) {
				ledGreenOnMillis = 0;
				digitalWrite(ledGreenPin, HIGH);
			}
		}
	}

	// Red LED
	if (ledRedEnable) {
		if (ledRedOnMillis == 0) {
			digitalWrite(ledRedPin, LOW);
		}
		ledRedOnMillis = millis();
	} else {
		if (ledRedOnMillis != 0) {
			if (millis() - ledRedOnMillis >= ledTime) {
				ledRedOnMillis = 0;
				digitalWrite(ledRedPin, HIGH);
			}
		}
	}
}
