# Arduino MIDI Roland SysEx Checksums
Use Arduino platform as a MIDI passthru to insert Roland SysEx checkums

For example, this can be used to control Roland XV MIDI expanders from old sofware that don't support Roland multi-bytes device ID SysEx checksums, like Cubase on Atari ST/TT/Falcon. 

Other features:
* Control XG Velocity Sense depth and offset for an external device (parameters read from potentiometers and sent as Sysex)
* Custom Thru Velocity Sense depth and offset (parameters read from potentiometers and applied in realtime on NoteOn messages)
* Configuration through custom System Exclusive messages

Uses the [FortySevenEffects's Arduino MIDI library](https://github.com/FortySevenEffects/arduino_midi_library)

Only tested with the [SparkFun MIDI shield](https://www.sparkfun.com/products/12898)
